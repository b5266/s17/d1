// JavaSript rray

// Array basic structures
/*Syntax
let/const arrayName = [ElementA, ElementB, etc..,];
*/

let grades =[98.5, 94.3, 89.2, 90.1];
let computerBrands =['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

//
let mixedArr =[12, 'Asus', null, undefined, true];

let myTasks = [
'drink html',
'eat javascript',
'inhale css',
'bake sass'
];

console.log(grades);
console.log(computerBrands[2]);
console.log(computerBrands[7]);



//
console.log('Array before reassignment');
console.log(myTasks);
myTasks[0] = 'Hello World';
console.log('Array after reassignment');
console.log(myTasks);

// Array Methods




let fruits = ['Apple', 'Orange', 'Kiwi','Dragon fruit'];

// push()


console.log('Current array:');
console.log(fruits);
let fruitslength = fruits.push('Mango');
console.log(fruitslength);
console.log('Mutated array from push method');
console.log(fruits);









let removedFruit = fruits.pop();
console.log(removedFruit);
console.log('Mutated array from pop method.');
console.log(fruits);

//unshift()
/*
	-adds one or more elements at the beginning of an array
	-syntax:
		arrayName.unshift('elementA');
		arrayName.unshift('elementA', 'elementB');
*/

fruits.unshift('Lime', 'Banana');
console.log('Mutated array from unshift method');
console.log(fruits);

// shift();
/*
	-removes an element at the beginning of the array and returns the removed element
	-Syntax:
		arrayName.shift()
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log('Mutated array from shift method');
console.log(fruits);

// splice();
/*
	-simultaneously removes elements from a specified index number and adds elements
	-syntax:
		arrayName.splice(startingIndex, deleteCount, elementToBeAdded);
*/
fruits.splice(1, 2, 'Lime', 'Cherry');
console.log('Mutated array from splice method:');
console.log(fruits);

// Banana, apple, orange, kiwi, dragon fruit

// sort();
/*
	-it rearranges the array elements in alphanumeric order
	- syntax:
		arrayName.sort();
*/

fruits.sort();
console.log('Mutated array from sort method:');
console.log(fruits);

// reverse();
/*
	- reverses the order of array elements
	-syntax:
		arrayName.reverse();
*/

fruits.reverse();
console.log('Mutated array from reverse method');
console.log(fruits);

// Non-mutator methods
// - these are functions that do not modify or change an array after they-re created

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

// IndexOf();
/*
	- returns na index number of the first matching element found in an array
	-if no match was found, the result will be -1.
	-syntax:
		arrayName.indexOf(searchValue);
*/
let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf method: ' + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Result of indexOf method: ' + invalidCountry);

// lastIndexOf
/*
	-returns the index number of the last matching element found in an array
	-syntax:
		arrayName.lastIndexOf(searchValue);
		arrayName.lastIndexOf(searchValue, fromIndex);
*/
// GEtting the index number starting from the last element 
let lastIndex = countries.lastIndexOf('PH');
console.log('Result of lastIndexOf method: '+ lastIndex);

// getting the index number starting from a specify a specified index
let lastIndexStart = countries.lastIndexOf('PH', 6);
console.log('Result of lastIndexOf method: ' + lastIndexStart);

// slice();
/*
	- portion/slices elements from an array and returns a new array
	- syntax:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingIndex);
*/

// ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];
// Slicing off elements from a specified index to the last element
let slicedArrayA = countries.slice(2);
console.log('Result from slice method: ');
console.log(slicedArrayA);

// slicing of elements from a specified index to another index
let slicedArrayB = countries.slice(2, 4);
console.log('Result from slice method: {}');
console.log(slicedArrayB);

// toString();
/*
	-returns an array as a string separated by commas
	-syntax:
		arrayName.toString();
*/

let stringArray = countries.toString();
console.log('Result from toString method: ');
console.log(stringArray);

// concat();
/*
	- combines 2 arrays and returns the combined result
	- syntax:
		arrayA.concat(arrayB);
		arrayA.concat(elementA);
*/

let tasksArrayA = ['drink html', 'eat javascript'];
let tasksArrayB = ['inhale css', 'breathe sass'];
let tasksArrayC = ['get git', 'be node'];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log('Result from cocat method: ');
console.log(tasks);

// combining multiple arrays
console.log('Result from concat method: ');
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

// combining array with elements
let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
console.log('Result from concat method: ');
console.log(combinedTasks);